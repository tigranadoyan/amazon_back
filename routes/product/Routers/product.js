const express = require("express");
const router = express.Router();
const {authMiddleware} = require('../../../utility/middlewares/accountMiddlewares');
const productController = require("../Controllers/product.js");

router.get("/options", productController.getOptions);

router.get("/", productController.getProducts);

router.get("/:id", productController.getProductById);

module.exports = router;
