const express = require('express');
const router = express.Router();
const {authMiddleware} = require('../../../utility/middlewares/accountMiddlewares');
const {uploadProductImages} = require('../../../utility/middlewares/productMiddlewares');
const storeController = require("../Controllers/store.js");

router.get("/", storeController.getUserProducts);

router.post("/", authMiddleware, storeController.createUserProduct);

router.patch('/img/:id', uploadProductImages, authMiddleware, storeController.updateUserProductImages);

router.patch("/", authMiddleware, storeController.updateUserProduct);

router.delete("/:id", authMiddleware, storeController.deleteUserProduct);


module.exports = router;
