const fs = require('fs');
const { User, Product, ProductImage, Color, Size, Category, ProductColor, ProductSize } = require("../../../DB/models");
const { ProductsGetter } = require("../../../utility/services/productService.js");
const {Op} = require("sequelize");

module.exports.getUserProducts = async (req, res) => {
    try {
        const {userId} = req.body;

        const products = await Product.findAll({
            where: {
                userId
            },
            include: [
                {model: Color, as: "colors", attributes: ['id', 'color'], through: {attributes: []}},
                {model: Size, as: "sizes", attributes: ['id', 'size'], through: {attributes: []}},
                {model: ProductImage, as: "images", attributes: ['id', 'productId', 'imageUrl']},
                {model: Category, as: "category", attributes: ["id", "parentId", "category"]}
            ],
            order: [
                ['createdAt', 'ASC']
            ]
        });

        res.json(products);
    } catch (e) {
        res.status(400).json(e)
    }
};

module.exports.createUserProduct = async (req, res) => {
  try {
      const { userId, name, description, brand, count, price, categoryId, colors, sizes } = req.body;

      const prevId = await Product.max('id');

      const values = {
          id: prevId + 1,
          userId: +userId,
          name,
          description,
          brand,
          count: +count,
          price: +price,
          categoryId: +categoryId,
      };

      const product = await Product.create(values);

      if (colors) {
          for (const colorId of colors) {
               await ProductColor.create({
                  productId: product.id,
                  colorId
              });
          }
      }

      if (sizes) {
          for (const sizeId of sizes) {
              await ProductSize.create({
                  productId: product.id,
                  sizeId
              })
          }
      }

      const result = await Product.findByPk(product.id, {
          include: [
              { model: Color, as: "colors", attributes: ['id', 'color'], through: {attributes: []} },
              { model: Size, as: "sizes", attributes: ['id', 'size'], through: {attributes: []} },
          ]
      });

      res.json(result);
  } catch (e) {
    res.status(400).json(e)
  }
};

module.exports.updateUserProduct = async (req, res) => {
    try {
        const {
            userId,
            id,
            imagesForRemove,
            name,
            description,
            brand,
            price,
            count,
            categoryId,
            publish,
            colors,
            sizes } = req.body;

        const updateValues = {};

        const product = await Product.findOne({
            where: {
                userId,
                id
            }
        })

        if (!product && product.userId !== userId ) {
            throw {
                code: 1,
                message: 'product not found !'
            }
        }

        // Info
        if (name) {
            updateValues.name = name;
        }
        if (description) {
            updateValues.description = description;
        }
        if (brand) {
            updateValues.brand = brand;
        }
        if (price) {
            updateValues.price = price;
        }
        if (count) {
            updateValues.count = count;
        }
        if (categoryId) {
            updateValues.categoryId = categoryId;
        }
        if (publish === true || publish === false) {
            updateValues.publish = publish;
        }

        // Update Colors and Sizes
        if (colors?.add.length) {
            for (const colorId of colors.add) {
                await ProductColor.create({
                    productId: id,
                    colorId
                })
            }
        }
        if (colors?.remove.length) {
            for (const colorId of colors.remove) {
                await ProductColor.destroy({
                    where: {
                        productId: id,
                        colorId,
                    },
                })
            }
        }
        if (sizes?.add.length) {
            for (const sizeId of sizes.add) {
                await ProductSize.create({
                    productId: id,
                    sizeId
                })
            }
        }
        if (sizes?.remove.length) {
            for (const sizeId of sizes.remove) {
                await ProductSize.destroy({
                    where: {
                        productId: id,
                        sizeId,
                    },
                })
            }
        }

        // Remove Images
        if (imagesForRemove?.length) {
            await ProductImage.destroy({
                where: {
                    productId: id,
                    id: {
                        [Op.in]: imagesForRemove
                    }
                }
            })
        }


        // Update Product
        if (Object.keys(updateValues).length) {
            await Product.update(updateValues, {
                where: {
                    id,
                    userId
                }
            })
        }

        res.json({
            result: true,
            message: 'Product Successfully updated !'
        })
    } catch (e) {
        res.status(400).json({
            message: 'Product update Error'
        })
    }
};

module.exports.updateUserProductImages = async (req, res) => {
    try {
        const { userId } = req.body;
        const { id } = req.params;

        const product = await Product.findOne({
            where: {
                userId,
                id
            }
        })

        if (!product) {
            throw {
                code: 1,
                message: 'Image upload error !'
            }
        }

        if (req.files.length) {
            const imageFiles = req.files;
/*
            const mainImage = imageFiles.shift();*/

            const images = await ProductImage.bulkCreate(req.files.map(image => ({
                productId: id,
                imageUrl: image.filename
            })));

            // const result = await Product.update({
            //     mainImage: mainImage.filename
            // }, {
            //     where: {
            //         id,
            //         userId,
            //     }
            // });

            debugger;

            res.json({
                images,
               // mainImage: result ? mainImage.filename : null,
                message: 'Images SuccessFully Updated'
            });
        } else {
            throw {
                code: 2,
                message: "Images upload error"
            }
        }
    } catch (e) {
        res.status(400).json(e)
    }
};

module.exports.deleteUserProduct = async (req, res) => {
    try {
        const { userId } = req.body;
        const { id } = req.params;

        const result = Product.destroy({
            where: {
                id: +id,
                userId
            }
        });

        if (!result) {
            throw {
                code: 1,
                message: 'Product remove !'
            }
        }

        res.json({
            result,
            message: "Product Successfully !"
        })
    } catch (e) {
        res.status(400).json(e)
    }
}