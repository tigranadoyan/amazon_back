const {
    User,
    Product,
    ProductImage,
    Color,
    Size,
    Category} = require("../../../DB/models");
const {ProductsGetter} = require("../../../utility/services/productService.js");

module.exports.getProducts = async (req, res) => {
    try {

        let {
            category = null,
            name = null,
            colors = null,
            sizes = null,
            maxP = 1000000,
            minP = 0,
            sortBy = "date",
            sort = "down",
            limit = 8,
            page = 1,
        } = req.query;

        if (colors) {
            colors = colors.split(",").map((el) => Number(el));
        }

        if (sizes) {
            sizes = sizes.split(",").map((el) => Number(el));
        }

        if (!sortBy || sortBy === "date" || sortBy !== "price" && sortBy !== "name") {
            sortBy = "updatedAt";
        }

        if (sort === "down" || !sort) {
            sort = "DESC";
        } else if (sort === "up") {
            sort = "ASC";
        }

        if (limit < 8) {
            limit = 8;
        } else if (limit > 50) {
            limit = 50;
        }

        const offset = (page - 1) * limit;

        // Getting Products

        const products = await ProductsGetter.getProducts({
            category: {id: category},
            name,
            colors,
            sizes,
            maxP,
            minP,
            sortBy,
            sort,
            limit,
            offset,
        });

        if ( category ) {
            const currentCategory = await Category.findByPk(category, {
                raw: true
            });
            var categories = await Category.getParentCategoriesRecursive(currentCategory);
        }



        res.json({
            products: products.rows,
            categories: categories ? categories : null,
            count: products.count
        });
    } catch (e) {
        res.status(400).json(e);
    }
};

module.exports.getProductById = async (req, res) => {
    try {
        const {id} = req.params;

        if (!id) {
            throw {
                code: 1,
                message: "Product id is empty"
            }
        }

        const product = await Product.findByPk(id, {
            include: [
                {model: User, as: "user", attributes: ["id", "firstName", "lastName", "email", "profileImageUrl"]},
                {model: ProductImage, as: "images", attributes: ["id", "imageUrl"]},
                {model: Color, as: "colors", attributes: ["id", "color"], through: {attributes: []}},
                {model: Size, as: "sizes", attributes: ["id", "size"], through: {attributes: []}},
                {model: Category, as: "category", attributes: ["id", "parentId", "category"]}
            ]
        });

        if (!product) {
            throw {
                code: 2,
                message: "Product not found !"
            }
        }

        const categories = await Category.getParentCategoriesRecursive({
            parentId: product.category.parentId,
            id: product.category.id,
            category: product.category.category
        });


        res.json({categories, product});
    } catch (e) {
        res.status(400).json(e);
    }
};

module.exports.getOptions = async (req, res) => {
    try {

        const categories = await Category.getSubCategoriesRecursive({id: null});

        const colors = await Color.findAll({
            attributes: ["id", "color"]
        });

        const sizes = await Size.findAll({
            attributes: ["id", "size"]
        });


        res.json({
            categories: categories.subCategories,
            colors,
            sizes
        });
    } catch (e) {
        res.json(e);
    }
};

