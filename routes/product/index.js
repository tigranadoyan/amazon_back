const express = require('express');
const router = express.Router();
const {authMiddleware} = require('../../utility/middlewares/accountMiddlewares');
const product = require('./Routers/product');
const store = require('./Routers/store');


router.use("/store", authMiddleware, store);

router.use("/", product);

module.exports = router;
