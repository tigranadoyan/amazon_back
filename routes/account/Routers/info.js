const express = require("express");
const {
  authMiddleware,
  updateAccountInfoValidationArray,
  updateUserAddressValidationArray,
  uploadProfileImage,
} = require("../../../utility/middlewares/accountMiddlewares.js");
const accountDataController = require("../Controllers/info");
const router = express.Router();

// Account Info

router.patch("/update", updateAccountInfoValidationArray, authMiddleware, accountDataController.updateInfo);

// Account Addresses

router.post("/address", authMiddleware, accountDataController.createUserAddress);

router.patch("/address", authMiddleware, updateUserAddressValidationArray, accountDataController.updateUserAddress);

router.delete("/address", authMiddleware, accountDataController.deleteUserAddress);

// updateAccount

router.patch("/image",  uploadProfileImage, authMiddleware, accountDataController.updateUserProfileImage);

module.exports = router;
