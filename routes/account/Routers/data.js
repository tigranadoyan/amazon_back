const express = require("express");
const {
    authMiddleware,
    updateCardProductsValidationArray,
} = require("../../../utility/middlewares/accountMiddlewares.js");
const accountDataController = require("../Controllers/data.js");
const router = express.Router();

// User Data

router.get("/", authMiddleware, accountDataController.getUserData);

// User Favorite

router.post("/favorites/:productId", authMiddleware, accountDataController.addUserFavorites);

router.delete("/favorites/:productId", authMiddleware, accountDataController.deleteUserFavorites);

router.get("/favorites", authMiddleware, accountDataController.getUserFavorites);

// Card Products

router.post("/card", updateCardProductsValidationArray, authMiddleware, accountDataController.updateCardProducts);

router.delete("/card", authMiddleware, accountDataController.deleteCardProducts);

router.get("/card", authMiddleware, accountDataController.getCardProducts);

// User Order

router.post("/order", authMiddleware, accountDataController.createUserOrder);

router.get("/order", authMiddleware, accountDataController.getUserOrders);

module.exports = router;
