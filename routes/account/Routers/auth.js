const express = require("express");
const {
    createValidationArray,
    loginValidationArray,
    sendConfirmValidationArray,
    resetPasswordValidationArray,
    authMiddleware,
} = require("../../../utility/middlewares/accountMiddlewares.js");
const accountController = require("../Controllers/auth.js");
const router = express.Router();

// Routers

router.post("/create", createValidationArray, accountController.createAccount);

router.post("/login", loginValidationArray, accountController.loginAccount);

router.post("/logout", accountController.logoutAccount);

router.get("/confirmation/:confirmToken", accountController.confirmEmail);

router.post("/confirmation", sendConfirmValidationArray, accountController.sendConfirmEmail);

router.post("/resetPassword", resetPasswordValidationArray, accountController.resetPassword);

router.delete("/delete", authMiddleware, accountController.deleteAccount);

module.exports = router;
