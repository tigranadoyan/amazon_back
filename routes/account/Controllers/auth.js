const {validationResult} = require("express-validator");
const {User} = require("../../../DB/models");
const {
    sendEmailConfirm,
    login,
    sendResetPassword,
} = require("../../../utility/services/accountService.js");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

module.exports.createAccount = async (req, res) => {
    try {
        const errors = validationResult(req).array({onlyFirstError: true});
        if (errors.length) {
            throw {code: 1, errors};
        }

        let {firstName, lastName, email, password} = req.body;

        const user = await User.create({
            firstName,
            lastName,
            email,
            password,
        })

        const confirmUrl = await sendEmailConfirm(user.id, user.email);

        res.json({
            id: user.id,
            email: user.email,
            confirmUrl,
        });
    } catch (e) {
        if (e.code === 1) {
            return res.status(401).json(e);
        }
        res.status(400).json(e);
    }
};

module.exports.loginAccount = async (req, res) => {
    try {
        const errors = validationResult(req).array({onlyFirstError: true});
        if (errors.length) {
            throw {code: 2, errors};
        }

        const {email, password} = req.body;

        const id = await login(email, password);


        const token = jwt.sign({id}, process.env.secretLoginJwt, {
            expiresIn: "2h",
        });
        res.json({
            message: "success",
            token
        });
    } catch (e) {
        if (e.code) {
            res.status(401).json(e);
        }
        res.status(400).json(e);
    }
};

module.exports.logoutAccount = async (req, res) => {
    try {
        res.clearCookie("token").json({
            message: 1,
        });
    } catch (e) {
        res.status(400).json(e);
    }
};

module.exports.confirmEmail = async (req, res) => {
    try {
        const {confirmToken} = req.params;

        const {id, email} = jwt.verify(
            confirmToken,
            process.env.secretEmailConfirmJwt
        );

        if (!id || !email) {
            throw {
                code: 1,
                message: "User not found !"
            };
        }

        const user = await User.update(
            {emailConfirmed: true},
            {where: {id, email}}
        ).then(async () => {
            return await User.findOne({
                where: {id, email},
                attributes: ["firstName", "lastName", "email", "profileImageUrl", "role"],
            });
        });

        if (!user) {
            throw {
                code: 2,
                message: "User is not found",
            };
        }


        res.json({message: "User email successfully confirmed !"})
    } catch (e) {
        if (e.code === 1) {
            res.status(404).json(e);
        }
        res.status(400).json(e);
    }
};

module.exports.sendConfirmEmail = async (req, res) => {
    try {
        const errors = validationResult(req).array({onlyFirstError: true});
        if (errors.length) {
            throw {code: 1, errors};
        }

        const {id, email} = req.body;

        const confirmUrl = await sendEmailConfirm(id, email);

        if (!confirmUrl) {
            throw {
                code: 2,
                message: "Email sending error !",
            };
        }

        res.json({
            confirmUrl
            });
        } catch (e) {
        res.status(400).json(e);
            }
};

module.exports.resetPassword = async (req, res) => {
    try {
        const errors = validationResult(req).array({onlyFirstError: true});
        if (errors.length) {
            throw {code: 1, errors};
        }

        const {email} = req.body;

        const newPassword = Math.random().toString(36).slice(-8);

        const resetPasswordUrl = await sendResetPassword(email, newPassword);

        if (!resetPasswordUrl) {
            throw {
                code: 2,
                message: "Password update error !",
            };
        }

        if (resetPasswordUrl) {
            const salt = await bcrypt.genSalt(10);
            const password = await bcrypt.hash(newPassword, salt);
            await User.update({password}, {where: {email}});
            res.json({
                resetPasswordUrl,
            });
        }
    } catch (e) {
        res.json(e);
    }
};

module.exports.deleteAccount = async (req, res) => {
    try {
        const {userId} = req.body;

        const result = await User.destroy({
            where: {id: userId},
            cascade: true,
        });

        res.json({
            message: result,
        });
    } catch (e) {
        res.json(e);
    }
};
