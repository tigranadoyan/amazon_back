const {
    User,
    UserAddress,
    UserFavorite,
    CardProduct,
    Product,
    UserOrder,
    OrderProduct,
} = require("../../../DB/models");
const {validationResult} = require("express-validator");

// Get User Data

module.exports.getUserData = async (req, res) => {
    try {
        const {userId} = req.body;

        const user = await User.findOne({
            where: {id: userId},
            attributes: ["firstName", "lastName", "email", "profileImageUrl"],
            include: [
                {
                    model: UserAddress,
                    as: "addresses",
                    attributes: ["id", "country", "city", "street", "isDefault"],
                },
                // {
                //     model: Product,
                //     as: "favorites",
                //     attributes: ["id"],
                //     through: {attributes: []}
                // },
                {
                    model: Product,
                    as: "card",
                    attributes: ["id", "name", "description", "brand", "mainImage", "price", "count", "deletedAt"],
                    through: { attributes: ["count"], as: "card" },
                    paranoid: false
                },
            ],
        }).then(data => data.toJSON());


        if (!user) {
            throw {
                code: 2,
                message: "User not found",
            };
        }

        if (user.card.length > 0) {
            user.card = user.card.map(({id, card: {count}}) => {
                return {
                    id, count
                }
            })
        }

        res.json(user);
    } catch (e) {
        res.status(400).json(e);
    }
};

// User Favorites

module.exports.addUserFavorites = async (req, res) => {
    try {
        const {userId} = req.body;
        const {productId} = req.params;

        if (!productId) {
            throw {
                code: 1,
                message: "Product not selected !",
            };
        }

        const result = await UserFavorite.create({
            userId,
            productId,
        });

        if (!result) {
            throw {
                code: 2,
                message: 0
            }
        }

        res.json({
            message: 1,
        });
    } catch (e) {
        res.status(400).json(e);
    }
};

module.exports.deleteUserFavorites = async (req, res) => {
    try {
        const {userId} = req.body;
        const {productId} = req.params;

        if (!productId) {
            throw {
                code: 1,
                message: "Product not selected !",
            };
        }

        const result = await UserFavorite.destroy({
            where: {
                userId,
                productId,
            },
        });

        res.json({
            message: result,
        });
    } catch (e) {
        res.status(400).json(e);
    }
};

module.exports.getUserFavorites = async (req, res) => {
    try {
        const {userId, fields} = req.body;

        const attr = [];
        if (!fields) {
            attr.push("id")
        } else {
            attr.push("id", "name", "brand", "price", "mainImage", "count")
        }

        const favorites = await UserFavorite.findAll({
            where: {userId},
            attributes: [],
            include: [
                {
                    model: Product,
                    as: "product",
                    attributes: attr,
                    paranoid: false
                },
            ],
        });

        const products = favorites.map(({product}) => {
            if (!fields) {
                return product.id;
            }
            return product;
        });

        const count = favorites.length;

        res.json({
            count,
            products,
        });
    } catch (e) {
        res.status(400).json(e);
    }
};

// Card Products

module.exports.updateCardProducts = async (req, res) => {
    try {
        const errors = validationResult(req).array({onlyFirstError: true});
        if (errors.length) {
            throw {code: 1, errors};
        }

        const {userId, productId, count} = req.body;

        const cardProduct = await CardProduct.findOne({
            where: {
                userId,
                productId
            }
        });

        let result = {};
        if (!cardProduct) {
            result = await CardProduct.create({userId, productId, count: 1});
        } else {
            if (count) {
                result = await CardProduct.update({count}, {where: {userId, productId}});
            } else {
                throw {
                    code: 3,
                    message: "Count is empty !"
                }
            }
        }

        res.json({
            message: result,
        });
    } catch (e) {
        res.status(400).json(e);
    }
};

module.exports.deleteCardProducts = async (req, res) => {
    try {
        const {userId, productId} = req.body;

        const result = await CardProduct.destroy({
            where: {
                userId,
                productId
            }
        });

        if (!result) {
            throw {
                code: 1,
                message: "Card Product not found"
            }
        }

        res.json({
            message: result
        });
    } catch (e) {
        res.status(400).json(e);
    }
};

module.exports.getCardProducts = async (req, res) => {
    try {
        const {userId} = req.body;

        const result = await User.findByPk(userId,{
                attributes: [],
                include: {
                    model: Product,
                    as: "card",
                    attributes: ["id", "name", "description", "brand", "mainImage", "price", "count", "deletedAt"],
                    through: { attributes: ["count"], as: "card" },
                    paranoid: false
                },
                order:[
                    ["id", "DESC"]
                ]
            });

        const { card } = result;


        res.json(card || []);
    } catch (e) {
        res.status(400).json(e);
    }
};

// User Order

module.exports.createUserOrder = async (req, res) => {
    try {
        const {userId, cardProducts} = req.body;

        const errors = {
            findErrors: [],
            countErrors: [],
        };

        let price = 0;

        for (let i = 0; i < cardProducts.length; i++) {
            const {id, count} = cardProducts[i]
            const product = await Product.findOne({where: {id}, paranoid: false, attributes: ["id", "name", "brand", "mainImage", "price", "count", "deletedAt"], raw: true})
            if (!product) {
                errors.findErrors.push({
                    product: {
                        id
                    },
                    message: "Product not found !"
                })
            } else if (product.deletedAt) {
                errors.findErrors.push({
                    product,
                    message: "Product are deleted !"
                })
            } else if (product.count < count) {
                errors.countErrors.push({
                    product,
                    message: `Max count is a ${product.count}`
                })
            } else if (product) {
                console.log(product.price, count)
                price += Number(product.price) * count
            }
        }

        if (errors.findErrors.length > 0 || errors.countErrors.length > 0) {
            throw {
                code: 1,
                message: errors
            }
        }

        const userOrder = await UserOrder.create({
            userId,
            price,
            status: "finished",
        });

        for (let i = 0; i < cardProducts.length; i++) {
            const {id, count} = cardProducts[i]
            await OrderProduct.create({orderId: userOrder.id, productId: id, count})
        }

        const result = await UserOrder.findOne({
            where: {id: userOrder.id},
            include: {model: Product, through: OrderProduct, as: "orderProducts"}
        })

        res.json(result)
    } catch (e) {
        res.status(400).json(e);
    }
};

module.exports.getUserOrders = async (req, res) => {
    try {
        const {userId} = req.body

        const result = await UserOrder.findAll({
            where: {userId},
            include: {model: Product, through: OrderProduct, as: "orderProducts"},
        });

        if (!result) {
            throw {
                code: 2,
                message: "User not found",
            };
        }

        res.json(result);
    } catch (e) {
        res.status(400).json(e);
    }
};
