const { User, UserAddress } = require("../../../DB/models");
const {
  uploadProfileImage,
} = require("../../../utility/middlewares/accountMiddlewares.js");
const bcrypt = require("bcrypt");
const { validationResult } = require("express-validator");

// Update Info

module.exports.updateInfo = async (req, res) => {
  try {
    const errors = validationResult(req).array({ onlyFirstError: true });
    if (errors.length) {
      throw { code: 1, errors };
    }
    let { userId, firstName, lastName, password, newPassword } = req.body;

    const user = await User.findByPk(userId);

    console.log("user id ", user.id);
    console.log("old password", password);
    console.log("new password", newPassword)

    if (!user) {
      throw {
        code: 3,
        message: "User not found !"
      }
    } else {
      if (newPassword) {
        const checkedPassword = await bcrypt.compare(password, user.password)
        if (!checkedPassword) {
          throw {
            code: 2,
            message: "Password is a wrong"
          }
        }
      }
    }

    const values = {};
    if (firstName) {
      values.firstName = firstName
    }
    if (lastName) {
      values.lastName = lastName
    }
    if (newPassword) {
      const salt = await bcrypt.genSalt(10);
      const hashedPassword = await bcrypt.hash(newPassword, salt);
      values.password = hashedPassword;
    }


    console.log(values)


    const result = await User.update(values, {
      where: {
        id: user.id
      },
      returning: true
    })

    console.log("result", result)

    if (result[0] !== 0) {
      res.json({
        message: "Successfully update",
      });
    } else {
      throw {
        code: 4,
        message: "Updating Error"
      }
    }
  } catch (e) {
    res.status(400).json(e);
  }
};

// User Addresses

module.exports.createUserAddress = async (req, res) => {
  try {
    const errors = validationResult(req).array({ onlyFirstError: true });
    if (errors.length) {
      throw { code: 1, errors };
    }

    let { userId, country, city, street, isDefault = false } = req.body;

    if (isDefault === true) {
      await UserAddress.update(
        { isDefault: false },
        { where: { userId, isDefault: true  } }
      );
    }

    const address = await UserAddress.create({
      userId,
      country,
      city,
      street,
      isDefault,
    });

    res.json(address);
  } catch (e) {
    res.status(400).json(e);
  }
};

module.exports.updateUserAddress = async (req, res) => {
  try {
    const errors = validationResult(req).array({ onlyFirstError: true });
    if (errors.length) {
      throw { code: 1, errors };
    }

    let { id, userId, country, city, street, isDefault = false } = req.body;

    if (isDefault === true) {
      await UserAddress.update(
        { isDefault: false },
        { where: { userId, isDefault: true } }
      );
    } else {
      isDefault = false;
    }

    const result = await UserAddress.update(
      { country, city, street, isDefault },
      { where: { id, userId } }
    );

    res.json({
      message: result,
    });
  } catch (e) {
    res.status(400).json(e);
  }
};

module.exports.deleteUserAddress = async (req, res) => {
  try {
    const errors = validationResult(req).array({ onlyFirstError: true });
    if (errors.length) {
      throw { code: 1, errors };
    }

    const { id, userId } = req.body;

    const result = await UserAddress.destroy({ where: { id, userId } });

    res.json({
      message: result,
    });
  } catch (e) {
    res.status(400).json(e);
  }
};

module.exports.getUserAddress = async (req, res) => {
  try {
    const errors = validationResult(req).array({ onlyFirstError: true });
    if (errors.length) {
      throw { code: 1, errors };
    }

    const { userId } = req.body;

    const userAddress = await UserAddress.findAll({ where: { userId } });

    res.json({
      userAddress,
    });
  } catch (e) {
    res.status(400).json(e);
  }
};

// Update User Profile Image

module.exports.updateUserProfileImage = async (req, res) => {
  try {
    const { filename } = req.file;
    const { userId } = req.body;

    const result = await User.update(
      { profileImageUrl: filename },
      { where: { id: userId } }
    );

    if (!result) {
      throw {
        code: 1,
        message: "Image update error !",
      };
    }

    res.json({
      profileImageUrl: filename,
    });
  } catch (e) {
    res.status(400).json(e);
  }
};
