const express = require("express");
const router = express.Router();
const auth = require("./Routers/auth.js");
const data = require("./Routers/data.js");
const info = require("./Routers/info.js")

// "account/"
// Account

router.use("/auth", auth);

router.use("/data", data);

router.use("/info", info);

module.exports = router;
