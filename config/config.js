module.exports = {
    development: {
        username: process.env.dbUsername,
        password: process.env.dbPassword,
        database: process.env.dbDatabase,
        host: process.env.dbHost,
        dialect: "postgres",
        logging: false
    },
    test: {
        username: "root",
        password: null,
        database: "database_test",
        host: "127.0.0.1",
        dialect: "mysql",
    },
    production: {
        username: "root",
        password: null,
        database: "database_production",
        host: "127.0.0.1",
        dialect: "mysql",
    },
};
