require("dotenv").config({path: __dirname + "/.env"});
const express = require("express");
const { sequelize } = require("./DB/models");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cors = require("cors");

const router = require("./routes/index");

const app = express();

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());
app.use(express.static("files"));


app.use(router)

async function connect() {
    try {
        const port = process.env["port"];
        await app.listen(port, (err) => {
            if (err) console.log("Error in Server Setup", err);
            sequelize.sync();
            console.log(`Server started successfully in port ${port}`);
        });
    } catch (error) {
        console.error("Unable to connect to the database:", error);
    }
}

connect();
