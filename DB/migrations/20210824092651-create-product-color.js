"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("productColors", {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      productId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "products",
          foreignKey: "id",
        },
        onDelete: "cascade",
      },
      colorId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "colors",
          foreignKey: "id",
        },
        onDelete: "cascade",
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("productColors");
  },
};
