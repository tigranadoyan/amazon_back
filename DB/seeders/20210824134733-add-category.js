"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("categories", [
      {
        parentId: null,
        category: "electronics",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parentId: 1,
        category: "phones",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parentId: 1,
        category: "notebooks",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("categories", null, {});
  },
};
