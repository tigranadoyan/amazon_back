"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("userOrders", [
      {
        userId: 1,
        price: 2300,
        status: "finished",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        userId: 2,
        price: 2500,
        status: "finished",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        userId: 3,
        price: 2900,
        status: "finished",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("userOrders", null, {})
  },
};
