"use strict";

const bcrypt = require("bcrypt");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash("password", salt);
    await queryInterface.bulkInsert(
      "users",
      [
        {
          firstName: "Tigran",
          lastName: "Adoyan",
          email: "tigran.adoyan@mail.ru",
          emailConfirmed: true,
          password: password,
          profileImageUrl: "user_1_Image.jpeg",
          role: "user",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          firstName: "Vagarshak",
          lastName: "Simonyan",
          email: "vagarshak.simonyan@mail.ru",
          emailConfirmed: true,
          password: password,
          profileImageUrl: "user_2_Image.jpeg",
          role: "user",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          firstName: "Anahit",
          lastName: "Galstyan",
          email: "anahit.galstyan@mail.ru",
          emailConfirmed: false,
          password: password,
          profileImageUrl: "user_3_Image.jpeg",
          role: "user",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("users", null, {});
  },
};
