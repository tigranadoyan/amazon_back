"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("productColors", [
      {
        productId: 1,
        colorId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 1,
        colorId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 1,
        colorId: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 2,
        colorId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 2,
        colorId: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 2,
        colorId: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 3,
        colorId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 3,
        colorId: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 3,
        colorId: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 4,
        colorId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 4,
        colorId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 4,
        colorId: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("productColors", null, {});
  },
};
