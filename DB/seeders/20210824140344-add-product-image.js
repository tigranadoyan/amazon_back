"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("productImages", [
      {
        productId: 1,
        imageUrl: `product_image_${Math.random()}_${Math.random()
          .toString(36)
          .substring(2, 7)}.jpeg`,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 1,
        imageUrl: `product_image_${Math.random()}_${Math.random()
          .toString(36)
          .substring(2, 7)}.jpeg`,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 2,
        imageUrl: `product_image_${Math.random()}_${Math.random()
          .toString(36)
          .substring(2, 7)}.jpeg`,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 2,
        imageUrl: `product_image_${Math.random()}_${Math.random()
          .toString(36)
          .substring(2, 7)}.jpeg`,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 3,
        imageUrl: `product_image_${Math.random()}_${Math.random()
          .toString(36)
          .substring(2, 7)}.jpeg`,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 3,
        imageUrl: `product_image_${Math.random()}_${Math.random()
          .toString(36)
          .substring(2, 7)}.jpeg`,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("productImages", null, {});
  },
};
