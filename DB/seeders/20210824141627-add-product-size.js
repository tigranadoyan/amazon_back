"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("productSizes", [
      {
        productId: 1,
        sizeId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 1,
        sizeId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 1,
        sizeId: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 2,
        sizeId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 2,
        sizeId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 2,
        sizeId: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 3,
        sizeId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 3,
        sizeId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 3,
        sizeId: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 4,
        sizeId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 4,
        sizeId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        productId: 4,
        sizeId: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("productSizes", null, {});
  },
};
