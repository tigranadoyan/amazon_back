"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("sizes", [
      {
        size: "small",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        size: "medium",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        size: "large",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("sizes", null, {});
  },
};
