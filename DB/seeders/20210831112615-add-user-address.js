"use strict";

const { DataTypes } = require("sequelize");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("userAddresses", [
      {
        userId: 1,
        country: "Armenia",
        city: "Erevan",
        street: "Qanaker",
        isDefault: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        userId: 1,
        country: "Armenia",
        city: "Exvard",
        street: "4 street",
        isDefault: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("userAddresses", null, {})
  },
};
