"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("colors", [
      {
        color: "red",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        color: "green",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        color: "yellow",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        color: "black",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        color: "white",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("colors", null, {});
  },
};
