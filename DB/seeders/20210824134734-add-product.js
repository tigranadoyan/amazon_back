"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      "products",
      [
        {
          userId: 1,
          name: "Iphone 11 pro",
          description: "Iphone 11 pro black",
          brand: "iphone",
          price: 700,
          mainImage: "product_1_main.jpeg",
          count: 10,
          categoryId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 1,
          name: "Samsung Galaxy 10",
          description: "Samsung Galaxy 10 black",
          brand: "samsung",
          price: 450,
          mainImage: "product_2_main.jpeg",
          count: 15,
          categoryId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 2,
          name: "Alcatel Mini 4",
          description: "Alcatel Mini 4 black",
          brand: "alcatel",
          price: 250,
          mainImage: "product_3_main.jpeg",
          count: 20,
          categoryId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          userId: 3,
          name: "HP ZenBook i5 2100Hz",
          description: 'HP ZenBook i5 2100Hz 17.6" 8GB RAM',
          brand: "hp",
          price: 950,
          mainImage: "product_4_main.jpeg",
          count: 17,
          categoryId: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("products", null, {});
  },
};
