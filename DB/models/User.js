"use strict";

const bcrypt = require("bcrypt");
const {Model, DataTypes} = require("sequelize");

module.exports = (sequelize) => {
    class User extends Model {
        static associate({
                     Product,
                     UserAddress,
                     UserFavorite,
                     CardProduct,
                     UserOrder,
                         }) {
            User.hasMany(Product, {
                foreignKey: "userId",
                as: "products",
            });

            User.hasMany(UserAddress, {
                foreignKey: "userId",
                as: "addresses",
            });

            User.belongsToMany(Product, {
                through: CardProduct,
                foreignKey: "userId",
                as: "card",
            });

            User.belongsToMany(Product, {
                through: UserFavorite,
                foreignKey: "userId",
                as: "favorites",
            });

            User.hasMany(UserOrder, {
                foreignKey: "userId",
                as: "userOrders",
            });
        }
    }

    User.init(
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            firstName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            lastName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
            },
            emailConfirmed: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            profileImageUrl: {
                type: DataTypes.STRING,
                unique: true,
            },
            role: {
                type: DataTypes.STRING,
                notNull: true,
                defaultValue: "user",
            },
        },
        {
            sequelize,
            paranoid: true,
            modelName: "User",
            tableName: "users",
        }
    );

    User.beforeCreate(async (user, options) => {
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
    });

    return User;
};
