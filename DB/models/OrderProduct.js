"use strict";

const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class OrderProducts extends Model {
    static associate() {
      //
    }
  }

  OrderProducts.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      orderId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      productId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      count: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "OrderProduct",
      tableName: "orderProducts",
    }
  );
  return OrderProducts;
};
