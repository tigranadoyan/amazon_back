"use strict";

const {Model, DataTypes} = require("sequelize");

module.exports = (sequelize) => {
    class CardProduct extends Model {
        static associate({Product, User}) {
            // CardProduct.belongsTo(Product, {
            //     foreignKey: "id",
            //     as: "product",
            // })
            // CardProduct.belongsTo(User, {
            //     foreignKey: "id",
            //     as: "user",
            // })
        }
    }

    CardProduct.init(
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            userId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            productId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            count: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
        },
        {
            sequelize,
            modelName: "CardProduct",
            tableName: "cardProducts",
        }
    );
    return CardProduct;
};
