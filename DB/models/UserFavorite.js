"use strict";

const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class UserFavorite extends Model {
    static associate({ User, Product }) {
      //
    }
  }

  UserFavorite.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      productId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "UserFavorite",
      tableName: "userFavorites",
    }
  );
  return UserFavorite;
};
