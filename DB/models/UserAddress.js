"use strict";

const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class UserAddress extends Model {
    static associate({ User }) {
      UserAddress.belongsTo(User, {
        foreignKey: "id",
        as: "addresses",
        onDelete: "cascade"
      });
    }
  }

  UserAddress.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      country: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      city: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      street: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      isDefault: {
        type: DataTypes.BOOLEAN,
      },
    },
    {
      sequelize,
      modelName: "UserAddress",
      tableName: "userAddresses",
    }
  );
  return UserAddress;
};
