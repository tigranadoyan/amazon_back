"use strict";

const {Model, DataTypes} = require("sequelize");

module.exports = (sequelize) => {
    class ProductImage extends Model {
        static associate({Product}) {
            ProductImage.belongsTo(Product, {
                foreignKey: "id",
                as: "product"
            })
        }
    }

    ProductImage.init(
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            productId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            imageUrl: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
            },
        },
        {
            sequelize,
            modelName: "ProductImage",
            tableName: "productImages",
        }
    );
    return ProductImage;
};
