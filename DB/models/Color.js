"use strict";

const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class Color extends Model {
    static associate({ Product, ProductColor }) {
      Color.belongsToMany(Product, {
        foreignKey: "colorId",
        through: ProductColor,
        as: "product",
      });
    }
  }

  Color.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      color: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Color",
      tableName: "colors",
    }
  );
  return Color;
};
