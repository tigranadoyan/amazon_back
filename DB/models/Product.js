"use strict";

const {Model, DataTypes} = require("sequelize");

module.exports = (sequelize) => {
    class Product extends Model {
        static associate({
                 User,
                 UserFavorite,
                 CardProduct,
                 UserOrder,
                 OrderProduct,
                 Category,
                 Color,
                 ProductColor,
                 Size,
                 ProductSize,
                 ProductImage,
            }) {
            Product.belongsTo(User, {
                foreignKey: "id",
                as: "user",
            });
            Product.belongsToMany(User, {
                through: CardProduct,
                foreignKey: "productId",
                as: "card",
            });
            Product.belongsToMany(User, {
                through: UserFavorite,
                foreignKey: "productId",
                as: "productFavorite",
            });
            Product.belongsToMany(UserOrder, {
                through: OrderProduct,
                foreignKey: "productId",
                as: "orderProduct",
            });
            Product.belongsTo(Category, {
                foreignKey: "categoryId",
                as: "category",
            });
            Product.belongsToMany(Color, {
                through: ProductColor,
                foreignKey: "productId",
                as: "colors"
            });
            Product.belongsToMany(Size, {
                through: ProductSize,
                foreignKey: "productId",
                as: "sizes",
            });
            Product.hasMany(ProductImage, {
                foreignKey: "productId",
                as: "images"
            });
        }
    }

    Product.init({
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            userId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            description: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            brand: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            price: {
                type: DataTypes.INTEGER,
            },
            mainImage: {
                type: DataTypes.STRING,
            },
            count: {
                type: DataTypes.INTEGER,
            },
            categoryId: {
                type: DataTypes.INTEGER,
                notNull: true,
            },
            publish: {
                type: DataTypes.BOOLEAN,
                defaultValue: false
            }
        },
        {
            sequelize,
            paranoid: true,
            modelName: "Product",
            tableName: "products",
        }
    );

    return Product;
};
