"use strict";

const {Model, DataTypes, Op} = require("sequelize");

module.exports = (sequelize) => {
    class Category extends Model {
        static associate({Category, Product, ProductCategory}) {
            Category.belongsTo(Category, {
                foreignKey: "id",
                as: "parent",
            });
            Category.hasMany(Category, {
                foreignKey: "parentId",
                as: "subCategories",
            });
            Category.hasMany(Product, {
                foreignKey: "categoryId",
                as: "products",
            });
        }
    }

    Category.init(
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            parentId: {
                type: DataTypes.INTEGER,
            },
            category: {
                type: DataTypes.STRING,
                allowNull: true,
            },
        },
        {
            sequelize,
            modelName: "Category",
            tableName: "categories",
        }
    );


    const getSubCategoriesRecursiveFunction = async (category) => {
        let subCategories = await Category.findAll({
            where: {
                parentId: category.id,
            },
            attributes: ["id", "category"],
            raw: true,
        });

        if (subCategories.length > 0) {
            const promises = [];
            subCategories.forEach((category) => {
                promises.push(getSubCategoriesRecursiveFunction(category));
            });
            category["subCategories"] = await Promise.all(promises);
        } else category["subCategories"] = [];
        return category;
    };

    Category.getSubCategoriesRecursive = getSubCategoriesRecursiveFunction;

    const getParentCategoriesRecursiveFunction = async (category) => {
        let parentCategory = await Category.findOne({
            where: {
                id: category.parentId,
            },
            attributes: ["id", "parentId", "category"],
            raw: true,
        });

        if (parentCategory) {
            category["parentCategory"] = await getParentCategoriesRecursiveFunction(
                parentCategory
            );
        } else category["parentCategory"] = [];
        return category;
    };

    Category.getParentCategoriesRecursive = getParentCategoriesRecursiveFunction;

    return Category;
};
