"use strict";

const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
    class ProductSize extends Model {
        static associate() {
            //
        }
    }

    ProductSize.init(
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
                allowNull: false,
            },
            productId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            sizeId: {
                type: DataTypes.STRING,
                allowNull: false,
            },
        },
        {
            sequelize,
            modelName: "ProductSize",
            tableName: "productSizes",
        }
    );
    return ProductSize;
};
