"use strict";

const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class UserOrder extends Model {
    static associate({ User, OrderProduct, Product }) {
      UserOrder.belongsTo(User, {
        foreignKey: "id",
        onDelete: "cascade",
      });
      UserOrder.belongsToMany(Product, {
        through: OrderProduct,
        foreignKey: "orderId",
        as: "orderProducts",
      });
    }
  }

  UserOrder.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      price: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "UserOrder",
      tableName: "userOrders",
    }
  );
  return UserOrder;
};
