"use strict";

const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class ProductColor extends Model {
    static associate() {
       //
    }
  }

  ProductColor.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      productId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      colorId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "ProductColor",
      tableName: "productColors",
    }
  );
  return ProductColor;
};
