"use strict";

const { Model, DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  class Size extends Model {
    static associate({ Product, ProductSize }) {
      Size.belongsToMany(Product, {
        foreignKey: "sizeId",
        through: ProductSize,
        as: "product",
      });
    }
  }

  Size.init(
    {
      id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
      },
      size: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Size",
      tableName: "sizes",
    }
  );

  return Size;
};
