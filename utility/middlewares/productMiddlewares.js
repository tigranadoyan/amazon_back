const multer = require("multer");

const multerProductImage = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "./files/images");
        },
        filename: function (req, file, cb) {
            const imageName = `${Date.now()}${Math.random()
                .toString(36)
                .slice(-8)}.${file.originalname.split(".").pop()}`;
            console.log('image name', imageName)
            cb(null, imageName);
        },
    }),

    fileFilter: function (req, file, callback) {
        if (file.mimetype !== "image/jpeg") {
            return callback(new Error("Only images are allowed"));
        }
        callback(null, true);
    },

}).array("image", 20);

module.exports.uploadProductImages = (req, res, next) => {
    multerProductImage(req, res, (err) => {
        if (err) {
            return res.status(400).send({message: err.message});
        }
        next();
    });
};
