const jwt = require("jsonwebtoken");
const multer = require("multer");

const {User, UserAddress, Product} = require("../../DB/models");
const {check} = require("express-validator");

// Account Auth

module.exports.authMiddleware = async (req, res, next) => {
    try {
        const token = req.headers.authorization;

        const {id} = await jwt.verify(
            token,
            process.env.secretLoginJwt
        );

        req.body.userId = id;
        next();
    } catch (e) {
        res.status(401).json({
            e
        });
    }
};

module.exports.createValidationArray = [
    check("firstName")
        .exists()
        .withMessage("First Name doesn't exist !")
        .notEmpty()
        .withMessage("First Name is empty !")
        .isString()
        .withMessage("First Name must be min 3 characters")
        .isLength({max: 20})
        .withMessage("First Name must be max 20 characters")
        .custom((value) => !/\s/.test(value))
        .withMessage("No spaces are allowed in the First Name"),
    check("lastName")
        .exists()
        .withMessage("Last Name doesn't exist !")
        .notEmpty()
        .withMessage("Last Name is empty !")
        .isString()
        .withMessage("Last Name must be min 3 characters")
        .isLength({max: 20})
        .withMessage("Last Name must be max 20 characters")
        .custom((value) => !/\s/.test(value))
        .withMessage("No spaces are allowed in the Last Name"),
    check("email")
        .exists()
        .withMessage("Email doesn't exist !")
        .notEmpty()
        .withMessage("Email is empty")
        .isEmail()
        .withMessage("Invalid email")
        .custom(async (email) => {
            await User.findOne({where: {email}}).then((user) => {
                if (user) {
                    throw new Error("User already exist");
                }
            });
            return true;
        }),
    check("password")
        .exists()
        .withMessage("Password doesn't exist !")
        .notEmpty()
        .withMessage("Password is a empty")
        .isLength({min: 6})
        .withMessage("Password must be min 6 characters")
        .isLength({max: 20})
        .withMessage("Password must be max 20 characters")
        .withMessage("No spaces are allowed in the Password"),
    check("checkPassword")
        .exists()
        .withMessage("checkPassword doesn't exist !")
        .notEmpty()
        .withMessage("Password confirmation is empty")
        .custom((value, {req}) => {
            if (value !== req.body.password) {
                throw new Error("Password confirmation is a wrong");
            }
            return true;
        }),
];

module.exports.loginValidationArray = [
    check("email")
        .exists()
        .withMessage("Email doesn't exist !")
        .isString()
        .withMessage("Email doesn't exist !")
        .notEmpty()
        .withMessage("Email is empty !"),
    check("password")
        .exists()
        .withMessage("Password doesn't exist !")
        .isString()
        .withMessage("Password doesn't exist !")
        .notEmpty()
        .withMessage("Password is empty !"),
];

module.exports.sendConfirmValidationArray = [
    check("id")
        .exists()
        .withMessage("Id doesn't exist !")
        .notEmpty()
        .withMessage("Id is empty"),
    check("email")
        .exists()
        .withMessage("Email doesn't exist !")
        .notEmpty()
        .withMessage("Email is empty"),
];

module.exports.resetPasswordValidationArray = [
    check("email")
        .notEmpty()
        .withMessage("Email is empty !")
        .custom(async (email) => {
            await User.findOne({where: {email}}).then((user) => {
                if (!user) {
                    throw new Error("User not found !");
                }
            });
            return true;
        }),
];

// Account Data

module.exports.updateCardProductsValidationArray = [
    check("productId")
        .notEmpty()
        .withMessage("Product is not selected !")
        .custom(async (productId) => {
            const product = await Product.findOne({
                where: {id: productId},
            });
            if (!product) {
                throw new Error("Product not found !");
            }
            return true;
        }),

];

// Account Info

module.exports.updateAccountInfoValidationArray = [
    check("newPassword").custom(async (newPassword, {req}) => {
        if (newPassword && newPassword.length < 6) {
            throw new Error("Min password length is a 6");
        }
        if (newPassword && newPassword !== req.body.checkPassword) {
            throw new Error("Check Password is a wrong !");
        }
    }),
];

module.exports.createUserAddressValidationArray = [
    check("country").notEmpty().withMessage("Country Field is Empty !"),
    check("city").notEmpty().withMessage("City Field is Empty !"),
    check("street").notEmpty().withMessage("Street field is empty !"),
];

module.exports.updateUserAddressValidationArray = [
    check("id")
        .notEmpty()
        .withMessage("Id doesn't exist !")
        .custom(async (id, {req}) => {
            const userAddress = await UserAddress.findOne({
                where: {id, userId: req.body.userId},
            });
            if (!userAddress) {
                throw new Error("User Address not found !");
            }
        }),
];

// Update Profile Image

const multerProfileImage = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "./files/images");
        },
        filename: function (req, file, cb) {
            const imageName = `${Date.now()}${Math.random()
                .toString(36)
                .slice(-8)}.${file.originalname.split(".").pop()}`;
            cb(null, imageName);
        },
    }),
    fileFilter: function (req, file, callback) {
        if (file.mimetype !== "image/jpeg") {
            return callback(new Error("Only images are allowed"));
        }
        callback(null, true);
    },
}).single("image");

module.exports.uploadProfileImage = (req, res, next) => {
    multerProfileImage(req, res, (err) => {
        if (err) {
            return res.status(400).send({message: err.message});
        }
        next();
    });
};
