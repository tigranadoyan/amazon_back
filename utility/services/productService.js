const {Op} = require("sequelize");
const {Product, Category, Color, Size} = require("../../DB/models");

module.exports.ProductsGetter = {
    productsCategoryId: [],
    getProducts: async function (filters) {

        this.productsCategoryId = [];

        const {category, name, colors, sizes, maxP, minP, sortBy, sort, limit, offset} = filters;

        if (category.id) {
            await this.getSubCategoriesRecursive(category);
        }

        const ProductCondition = {
            publish: true
        };

        if (maxP || minP) {
            ProductCondition["price"] = {
                [Op.or]:[
                    {[Op.between]: [minP, maxP]},
                    null
                ],
            };
        }

        if (category.id) {
            ProductCondition["categoryId"] = {[Op.in]: [...this.productsCategoryId]};
        }

        if (name) {
            ProductCondition["name"] = {
                [Op.iLike]: `%${name}%`,
            }
        }

        const include = [];

        if (colors) {
            include.push({
                model: Color,
                attributes: [],
                as: "colors",
                through: {attributes: []},
                where: {
                    id: {[Op.in]: colors}
                }
            })
        }

        if (sizes) {
            include.push({
                model: Size,
                attributes: [],
                as: "sizes",
                through: {as: "sizes", attributes: []},
                where: {
                    id: {[Op.in]: sizes}
                },
            })
        }

        return await Product.findAndCountAll({
            where: ProductCondition,
            order: [
                [sortBy, sort],
            ],
            offset,
            limit,
            include,
        });
    },

    getSubCategory: async function(category) {
        return await Category.findAll({
            where: {
                parentId: category.id,
            },
            attributes: ["id", "category"],
            raw: true,
        });
    },

    getSubCategoriesRecursive: async function (category) {
        const subCategories = await this.getSubCategory(category);

        if (subCategories.length > 0) {
            const promises = [];
            for (const category of subCategories) {
                   const subCategories = await this.getSubCategory(category);
                   if (subCategories) {
                       promises.push(this.getSubCategoriesRecursive(category));
                   } else {
                       this.productsCategoryId.push(category.id);
                   }
            }
            category["subCategories"] = await Promise.all(promises);
        } else {
            this.productsCategoryId.push(category.id)
        }
    },
};
