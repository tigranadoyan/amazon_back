const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
const {User} = require("../../DB/models");

module.exports.login = async (email, password) => {
    try {
        const user = await User.findOne({
            where: {email},
            raw: true,
        });

        if (!user) {
            throw {
                code: 1,
                message: "Email or password is wrong !",
            };
        }

        const checkedPassword = await bcrypt.compare(password, user.password);

        if (!checkedPassword) {
            throw {
                code: 1,
                message: "Email or password is wrong !",
            };
        }

        if (!user.emailConfirmed) {
            throw {
                code: 3,
                message: "Please confirm email !",
                id: user.id,
                email: user.email,
            };
        }

        if (checkedPassword) {
            return user.id;
        }
    } catch (e) {
        console.log(e);
        throw e;
    }
};

module.exports.sendEmailConfirm = async (id, email) => {
    const token = jwt.sign({id, email}, process.env.secretEmailConfirmJwt, {
        expiresIn: "2h",
    });

    let testAccount = await nodemailer.createTestAccount();
    console.log("main", testAccount);

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false,
        auth: {
            user: testAccount.user,
            pass: testAccount.pass,
        },
    });

    const url = `http://localhost:3000/confirm/${token}`;
    console.log(url);
    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        to: email, // list of receivers
        subject: "Amazon", // Subject line
        text: "Amazon", // plain text body
        html: `<h1>Please click link to confirm your email <a href="${url}" target="_blank"> Confirm </a> </h1>`, // html body
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    console.log(token);
    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    return nodemailer.getTestMessageUrl(info);
};

module.exports.sendResetPassword = async (email, password) => {
    let testAccount = await nodemailer.createTestAccount();
    console.log("main", testAccount);

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: testAccount.user, // generated ethereal user
            pass: testAccount.pass, // generated ethereal password
        },
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        to: email, // list of receivers
        subject: "Amazon", // Subject line
        text: "Amazon", // plain text body
        html: `<h1> Your new password is a :  ${password} </h1>`, // html body
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    return nodemailer.getTestMessageUrl(info);
};
